import { LitElement, html, css } from 'lit-element';

export class SearchForm extends LitElement {

    static get properties() {
        return {
            value: { type: String }
        }
    }

    static get styles() {
        return css`
            *{
                padding: 0;
                margin: 0;
                box-sizing: border-box;
            }

            form {
                font-family: Arial, Helvetica, sans-serif;
                height: 30px;
                margin-bottom: 10px;
            }

            input {
                height: 100%;
            }

            button {
                height: 100%;
                padding: 5px;
                border-radius: 0px;
                border: 1px solid black;
            }
        `;
    }

    constructor() {
        super();

        this.value = '';
    }

    __triggerEvent(eventName, detail, bubbles = false, composed = false) {
        this.dispatchEvent(new CustomEvent(eventName, {
            detail: detail,
            bubbles: bubbles,
            composed: composed
        }));
    }

    __onClickSearch() {
        this.value = this.shadowRoot.querySelector('#search-box').value;
        this.__triggerEvent('search-employee', this.value, true, true);
    }

    __onClickClean() {
        this.__triggerEvent('clean-form', null, true, true);
    }

    render() {
        return html`
            <form>
                <label for="search-box">Filtrar</label>
                <input type="text" id="search-box">
                <button type="button" id="btn-search" @click="${this.__onClickSearch}">Buscar</button>
                <button type="reset" @click="${this.__onClickClean}">Limpiar</button>
            </form>
        `;
    }
}

customElements.define('search-form', SearchForm);
