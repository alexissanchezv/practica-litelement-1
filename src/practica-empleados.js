import { LitElement, html, css } from 'lit-element';
import { SearchForm } from './search-form';

export class PracticaEmpleados extends LitElement {

    static get properties() {
        return {
            employees: { type: Array },
            orderedEmployees: { type: Array },
            message: { type: String },
            order: { type: String }
        }
    }

    static get styles() {
        return css`
            * {
                padding: 0;
                margin: 0;
                box-sizing: border-box;
            }

            :host {
                font-family: Arial, Helvetica, sans-serif;
            }

            table {
                width: 100%;
                border-collapse: collapse;
                margin-bottom: 10px;
            }

            table thead {
                border-bottom: 1px solid black;
                border-top: 1px solid black;
                text-align: left;
                height: 35px;
            }
            table tr {
                border-bottom: 1px solid black;
                height: 25px;
            }

            figcaption {
                text-align: start;
                font-size: 15px;
            }

            .order-by {
                cursor: pointer;
            }
        `;
    }

    constructor() {
        super();
        this.employees = [];
        this.orderedEmployees = [];
        this.message = '';
        this.order = '';
    }

    connectedCallback(){
        super.connectedCallback();

        this.message = 'Buscando datos...';
        fetch('http://dummy.restapiexample.com/api/v1/employees')
            .then(response => {
                if (response.ok) return response.json();
                
                return Promise.reject(response);
            })
            .then(data => {
                this.employees = data.data;
                this.__sortEmployees(this.employees);
                this.message = (this.employees.length === 0) ? 'No hay datos' : '';
            })
            .catch((err) => {
                this.message = `Error ${err.status}: ${err.statusText}`;
                console.error(`Error ${err.status}: ${err.statusText}`);
            });
    }

    __sortEmployees(data = null, order = '') {
        data = (data) ? data : this.orderedEmployees;
        this.order = order;

        switch(this.order.toUpperCase()) {
            case 'ASC':
                this.orderedEmployees = data.sort(this.__sortAsc);
                break;
            case 'DESC':
                this.orderedEmployees = data.sort(this.__sortDesc);
                break;
            default:
                this.orderedEmployees = data;
                break;
        }
    }

    __sortAsc(a, b) {
        if (a.employee_name > b.employee_name) return 1;
        if (a.employee_name < b.employee_name) return -1;
        return 0;
    }

    __sortDesc(a, b) {
        if (a.employee_name < b.employee_name) return 1;
        if (a.employee_name > b.employee_name) return -1;
        return 0;
    }

    __orderBy() {
        this.order = (this.order == 'ASC') ? 'DESC' : 'ASC';
        this.__sortEmployees(null, this.order);
    }

    __searchEmployee(e) {
        const employeesFound = this.employees.filter(employee => (new RegExp(e.detail,'i')).test(employee.employee_name));
        this.__sortEmployees(employeesFound);
    }

    __resetEmployees() {
        this.orderedEmployees = this.employees;
    }

    render() {
        return html`
        <search-form  @clean-form="${this.__resetEmployees}" @search-employee="${this.__searchEmployee}"></search-form>
        <table>
            <thead>
                <tr>
                    <th>#</th>
                    <th @click="${this.__orderBy}" class="order-by">Nombre</th>
                    <th>Salario</th>
                    <th>Edad</th>
                    <th>Imagen</th>
                </tr>
            </thead>
            <tbody>
                ${(this.employees.length > 0)
                    ? html`
                    ${this.orderedEmployees.map((employee) => 
                        html`<tr>
                                <td>${employee.id}</td>
                                <td>${employee.employee_name}</td>
                                <td>${employee.employee_salary}</td>
                                <td>${employee.employee_age}</td>
                                <td>${employee.profile_image}</td>
                            </tr>`
                        )}`
                    : html` <tr>
                                <td colspan="5">${this.message}</td>
                            </tr>`}
            </tbody>
        </table>`;
    }

}

customElements.define('practica-empleados', PracticaEmpleados);
